package edu.nctu.wirelab.measuring.Connect;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import edu.nctu.wirelab.measuring.Fragment.LoginFragment;
import edu.nctu.wirelab.measuring.Fragment.LoginUserFragment;
import edu.nctu.wirelab.measuring.SocialUserConfig;
import edu.nctu.wirelab.measuring.UserConfig;
import edu.nctu.wirelab.measuring.File.JsonParser;
import edu.nctu.wirelab.measuring.MainActivity;
import edu.nctu.wirelab.measuring.ShowDialogMsg;

import static edu.nctu.wirelab.measuring.Connect.Setting.HTTPS_SERVER;

/**
 * Send the signal data to specified server path by GET or POST method
 * It's newed in
 * 1. MainActivity: when an user login, he/she send a http request
 * 2. UserFragment: when the user upload the signal data
 */

public class HttpsConnection extends AsyncTask<String, Void, String> {
    private static final String tagName = "HttpsConnection";
    public static String ftpKey = "";
    private Activity mActivity = null;
    private Fragment mFragment;

    private Certificate ca;
    private Context myContext;
    private JsonParser JsonParser = null;
    private SSLContext sslContext;
    private String configPath;
    private String sendMethod = null; //sendMethod can be "GET" or "POST"
    private String getVariables = null, postVariables = null;
    private String myUserName = null;
    private String mCode = null;
    private String resultKey = "";
    public DialogInterface dialog;
    ProgressDialog progressDialog;

    public void setJsonParser(JsonParser json){
        JsonParser=json;
    }

    public void setDialogInterface(DialogInterface dialogInterface){
        dialog = dialogInterface;
    }
    public HttpsConnection(Context context){
        myContext = context;
        progressDialog = new ProgressDialog(myContext);
        try {
            configPath = "/data/data/" + myContext.getPackageName() + "/config";

            // Load CAs from an InputStream
            // (could be from a resource or ByteArrayInputStream or ...)
            CertificateFactory cf = CertificateFactory.getInstance("X.509");

            // Need remembering to copy apache.pem file in app/main/Assets folder!!!
            // Ref: https://www.myandroid.tw/bbs-topic-1606-0.sea
            InputStream caInput = new BufferedInputStream(myContext.getAssets().open("apache2.pem"));

            ca = cf.generateCertificate(caInput);
            caInput.close();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            // Create an SSLContext that uses our TrustManager
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, tmf.getTrustManagers(), null);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setMessage("Loading...");
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        URL url = null;
        String urlFile = "";
        String responseStr="";
        try {
            if(sendMethod!=null && sendMethod.equals("GET")){
                urlFile = params[0].concat("?" + getVariables);
            }
            else{
                urlFile = params[0];
            }

            // Tell the URLConnection to use a SocketFactory from our SSLContext
            url = new URL("https", HTTPS_SERVER, urlFile); // ("https", "140.113.216.37", "/abc/def") -> https://140.113.216.37/abc/def
            Log.d(tagName, "urlFile:" + url);

            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setSSLSocketFactory(sslContext.getSocketFactory());
            urlConnection.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session){
                    Log.d("0513 host: ", hostname );
                    return true;
                }
            });
            if(sendMethod!=null && sendMethod.equals("GET")){
                urlConnection.setRequestMethod("GET");
            }
            else if(sendMethod!=null && sendMethod.equals("POST")){
                urlConnection.setRequestMethod("POST");
                DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
                wr.writeBytes(postVariables);
                wr.flush();
                wr.close();
            }

            InputStream in = urlConnection.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));


            String line;
            while ((line = reader.readLine()) != null) {
                //Log.d(tagName, "line:" + line);
                responseStr = responseStr.concat(line);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "connection failed";
        } catch (IOException e) {
            //Log.d(tagName, "The website may be crashed");
            e.printStackTrace();
            return "connection failed";
        }
        return responseStr;
    }

    public void setMethod(String method, String variables){
        sendMethod = method;
        Log.d("split1", variables);
        splitAndGetVariables(variables);
        Log.d("split2", variables);

        if(sendMethod.equals("GET")){
            getVariables = variables;
            postVariables = null;
        }
        else if(sendMethod.equals("POST")){
            postVariables = variables;
            getVariables = null;
            Log.d("0504: ",postVariables);
        }
        else{
            sendMethod = null;
            postVariables = null;
            getVariables = null;
        }
    }

    public void splitAndGetVariables(String variables){
        String[] token = variables.split("&");

        for(String t : token){
            //Log.d(tagName, "t: " + t);
            if(t.contains("username")){
                Log.d(tagName, "t:"+t);
                String[] subtoken = t.split("=");
                myUserName = subtoken[1];
                Log.d(tagName, "myUserName:"+myUserName);
            }
            else if (t.contains("code")){
                String[] subtoken = t.split("=");
                mCode = subtoken[1];
                Log.d(tagName, "t: "+mCode);
            }
        }
    }

    public void setActivity(Activity activity){
        mActivity = activity;
    }

    public void setFragment(Fragment fragment){
        mFragment = fragment;
    }


    public String getResultKey(){
        return resultKey;
    }

    private void login(){
        UserConfig.setUserName(myUserName);
        UserConfig.saveConfigTo(configPath);
        MainActivity.recordPrefix = new String (UserConfig.myUserName+"RECORD");
        Log.d("0523", "userName:"+myUserName);
        JsonParser.setAccount(myUserName);
        if(mActivity instanceof MainActivity){
            ((MainActivity) mActivity).loginSuccess();
        }
    }

    public void unpackJson(JSONArray jsonArray){

        try {
            //Log.d("0717","unpackJson in Https");

            JSONObject jsonObject = jsonArray.getJSONObject(0);
            Log.d("0717","in Https"+ jsonObject.toString());

            if (jsonObject.has("userid")) { // social list
                Log.d("0612", "setSocialJson");
                UserConfig.setSocialInfo(jsonArray);
                if(mActivity instanceof MainActivity){
                    ((MainActivity) mActivity).transferSocial();
                }
            }


            else if (jsonObject.has("username")){ // userinfo (gender, email)
                if(jsonObject.getString("username").toString().compareTo(UserConfig.myUserName)==0) { // the user him/herslef
                    UserConfig.setUserInfo(jsonObject);
                    if(mActivity instanceof MainActivity){
                        ((MainActivity) mActivity).transferLoginUser();
                    }
                }
                else { // the user's similar user
                    SocialUserConfig.setSocialUserInfo(jsonObject);
                    if(mActivity instanceof MainActivity){
                        ((MainActivity) mActivity).transferSimUser();
                    }
                }
            }

        }

        catch (JSONException e){
            e.printStackTrace();
        }
    }

    // result: response message from server
    protected void onPostExecute(String result) {
        Log.d("0612json", result);
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        try {
            //JSONObject json = new JSONObject(result);
            JSONArray json = new JSONArray(result);
            Log.d("0523jsons", json.toString());

            unpackJson(json);
            return;

        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.d("0508 from server: ",result );
        if(result.compareTo("create a user successfully")==0){
        }
        else if(result.compareTo("login successfully")==0){
            ShowDialogMsg.showDialog(result);
            login();
        }
        else if(result.compareTo("UserID is error")==0){
            Toast toast = Toast.makeText(myContext, result, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();

        }
        else if(result.compareTo("Password is error")==0){
            Toast toast = Toast.makeText(myContext, result, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
        }


        else if (result.compareTo("register successfully")==0){
            //((LoginFragment) mFragment).waitForRegister(0, dialog);
            Log.d("0523", "post success");

            login();
        }
        else if (result.compareTo("register unsuccessfully")==0){
            //((LoginFragment) mFragment).waitForRegister(1, dialog);
            Log.d("0523", "post unsuccess");
            result = "The userid has been used";
            ShowDialogMsg.showDialog(result);
        }

        else{ // for app version
            Log.d("0504 from server: ",result );
            Log.d(tagName,"APPVERSION: " + MainActivity.APPVERSION);
            if (result.compareTo(MainActivity.APPVERSION)==1){
                //http://www.mysamplecode.com/2013/05/android-update-application.html

                Log.d(tagName,"APPVERSION: " + MainActivity.APPVERSION);

                AlertDialog alertDialog = new AlertDialog.Builder(myContext)
                        .setTitle("Warning")
                        .setMessage("You have to update the latest app version!")
                        .setPositiveButton("Update", new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which){
                                Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=edu.nctu.wirelab.measuring"));
                                marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET | Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                mActivity.startActivity(marketIntent);
                            }
                        }).show();
                result = "";
            }
             if(sendMethod!=null && sendMethod.equals("POST")){
                resultKey = result;
                Log.d(tagName,"key: " + result);
                ftpKey = result;
                result = "";
            }
            else{
                //ShowDialogMsg.showDialog(result);
            }
        }


    }
}
