package edu.nctu.wirelab.measuring.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

import edu.nctu.wirelab.measuring.R;
import edu.nctu.wirelab.measuring.SocialUserConfig;

/**
 * Created by py on 6/17/18.
 */

public class SimUserFragment extends Fragment{
    private ListView listView;
    SimpleAdapter adapter;
    ArrayList<HashMap<String,String>> items;

    final String ID_TITLE = "TITLE", ID_SUBTITLE = "SUBTITLE";
    private String [] infoItems = new String [] {"UserID", "Gender", "Birthday", "E-mail", "Introduction"};

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.listview_social, container, false);
        listView = (ListView) view.findViewById(R.id.list);

        items = new ArrayList<HashMap<String, String>>();
        for(int i=0; i<SocialUserConfig.infoValue.length; i++) {
            HashMap<String, String> item = new HashMap<String, String>();
            item.put(ID_TITLE, infoItems[i]);
            item.put(ID_SUBTITLE, SocialUserConfig.infoValue[i]);
            items.add(item);
        }

       adapter = new SimpleAdapter(
                getActivity(),
                items,
                android.R.layout.simple_list_item_2,
                new String[]{ID_TITLE, ID_SUBTITLE},
                new int[]{android.R.id.text1, android.R.id.text2}
       );


        listView.setAdapter(adapter);

        return  view;
    }


}
