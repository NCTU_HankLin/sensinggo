package edu.nctu.wirelab.measuring.Fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import java.lang.reflect.Field;

import edu.nctu.wirelab.measuring.CheckFormat;
import edu.nctu.wirelab.measuring.Connect.HttpsConnection;
import edu.nctu.wirelab.measuring.File.JsonParser;
import edu.nctu.wirelab.measuring.R;
import edu.nctu.wirelab.measuring.ShowDialogMsg;

/**
 * Created by py on 4/17/18.
 */

@RequiresApi(api = Build.VERSION_CODES.N)
public class LoginFragment extends Fragment{
    private Button loginBtn, registerBtn;
    private EditText userNameEditText, pwdEditText;
    public Context mContext;


    DialogInterface alertDialog;


    private JsonParser jsonParser = null;
    public void setJsonParser(JsonParser json) {
        jsonParser = json;
    }

    public void dialogNoDismiss(DialogInterface dialog){
        try {
            Field field = dialog.getClass().getSuperclass().getDeclaredField("mShowing");
            field.setAccessible(true);
            field.set(dialog, false);
        }
        catch (Exception e) {
        }
    }

    public void dialogDismiss(DialogInterface dialog){
        try {
            Field field = dialog.getClass().getSuperclass().getDeclaredField("mShowing");
            field.setAccessible(true);
            field.set(dialog, true);
        }
        catch (Exception e) {
        }
    }

    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        loginBtn = (Button) view.findViewById(R.id.LoginBtn);
        registerBtn = (Button) view.findViewById(R.id.RegisterBtn);
        userNameEditText = (EditText) view.findViewById(R.id.userNameEditText);
        pwdEditText = (EditText) view.findViewById(R.id.pwdEditText);


        loginBtn.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                String userName = userNameEditText.getText().toString();
                String pwd = pwdEditText.getText().toString();
                if(!userName.equals("") && !pwd.equals("")) {

                    String loginInfo = "username=" + userName +"&password=" + pwd;
                    HttpsConnection httpsConnection = new HttpsConnection(getActivity());
                    httpsConnection.setJsonParser(jsonParser);
                    httpsConnection.setActivity(getActivity());
                    httpsConnection.setMethod("POST", loginInfo);
                    httpsConnection.execute("/login");//exec with the url, such as https://140.113.216.37/login;
                }
                else{
                    ShowDialogMsg.showDialog("the field can't be blank");
                }
            }
        });

        registerBtn.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                final View registerView =  LayoutInflater.from(getActivity()).inflate(R.layout.fragment_register,null);

                alertDialog = new AlertDialog.Builder(getActivity())
                    .setTitle("Register")
                    .setView(registerView)
                    .setNegativeButton("Cancel",new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface dialog, int which){
                            dialogDismiss(dialog);
                        }
                    })
                    .setPositiveButton("Register", new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface dialog, int which){
                            EditText userIdEditText = (EditText) registerView.findViewById(R.id.registerID);
                            EditText pwdEditText = (EditText) registerView.findViewById(R.id.registerPwd);
                            EditText mailEditText = (EditText) registerView.findViewById(R.id.registerMail);
                            EditText birthEditText = (EditText) registerView.findViewById(R.id.registerBirth);
                            EditText greetEditText = (EditText) registerView.findViewById(R.id.hello);
                            RadioGroup rg = (RadioGroup) registerView.findViewById(R.id.genderGroup);



                            String userId = userIdEditText.getText().toString();
                            String pwd = pwdEditText.getText().toString();
                            String mail = mailEditText.getText().toString();
                            String birth = birthEditText.getText().toString();
                            String msg = greetEditText.getText().toString();
                            String gender = "";

                            switch (rg.getCheckedRadioButtonId()){
                                case R.id.male:
                                    gender = "male";
                                    break;
                                case R.id.female:
                                    gender = "female";
                                    break;
                            }

                            if(userId.equals("") || pwd.equals("") || mail.equals("")
                                    || birth.equals("") || gender.equals("")){
                                //dialogNoDismiss(dialog);
                                /*try {
                                    Field field = dialog.getClass().getSuperclass().getDeclaredField("mShowing");
                                    field.setAccessible(true);
                                    field.set(dialog, false);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }*/
                                ShowDialogMsg.showDialog("the * field can't be blank");
                            }
                            else if (!CheckFormat.checkEmail(mail)){
                                ShowDialogMsg.showDialog("Please enter a valid e-mail.");
                            }
                            else if (!CheckFormat.checkBirth(birth)){
                                ShowDialogMsg.showDialog("Please enter a valid birthday.");
                            }

                            else{

                                String registerInfo = "username=" + userId +
                                        "&password=" + pwd +
                                        "&gender=" + gender +
                                        "&birthday=" + birth +
                                        "&email=" + mail +
                                        "&helloMsg=" + msg;
                                //dialogNoDismiss(dialog);

                                final HttpsConnection httpsConnection = new HttpsConnection(getActivity());
                                httpsConnection.setJsonParser(jsonParser);
                                httpsConnection.setMethod("POST", registerInfo);
                                httpsConnection.setFragment(LoginFragment.this);
                                httpsConnection.setActivity(getActivity());
                                httpsConnection.setDialogInterface(dialog);
                                httpsConnection.execute("/register");

                            }
                        }
                    }).show();
            }

        });

        return  view;
    }




    // wait for the background process finish, if register is not success, not dismiss the dialog, o.w. dismiss it
    /*public void waitForRegister(int status, DialogInterface dialog){ // wait for 3 cycles
        if(status==0){ // register successfully
            Log.d("0523", "wait success");
            dialogDismiss(dialog);
        }
        else if (status==1){
            Log.d("0523", "wait unsuccess");
            dialogNoDismiss(dialog);
        }
    }*/


}
