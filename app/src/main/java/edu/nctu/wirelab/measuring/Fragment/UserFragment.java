package edu.nctu.wirelab.measuring.Fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import edu.nctu.wirelab.measuring.Connect.SFTPController;
import edu.nctu.wirelab.measuring.MainActivity;
import edu.nctu.wirelab.measuring.RunIntentService;
import edu.nctu.wirelab.measuring.UserConfig;
import edu.nctu.wirelab.measuring.Connect.HttpsConnection;
import edu.nctu.wirelab.measuring.R;
import edu.nctu.wirelab.measuring.ShowDialogMsg;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserFragment extends Fragment {
    public static TextView uploadProgress, runningText;
    public static boolean tempAutoUploadByMobile;

    private Button uploadButton, quitButton;
    private ProgressBar pbUpload;
    private TextView idTextView;
    private TextView pbText;
    private CheckBox autoCheckBox;

    SimpleDateFormat sdf;
    SimpleDateFormat sdfMilli;
    Date LogDate;
    Context mContext;

    private Handler updateViewHandler = new Handler();

    public UserFragment() {
    }

    //-------------------
    public void SetContext(Context ctx) {
        mContext = ctx;
    }

    Runnable updateView = new Runnable() {
        @Override
        public void run() {
            idTextView.setText("Hi, " + UserConfig.myUserName);
            updateViewHandler.postDelayed(updateView, 2000);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user, container, false);

        idTextView = (TextView) view.findViewById(R.id.IDTextView);
        runningText = (TextView) view.findViewById(R.id.runningText);
        uploadProgress = (TextView) view.findViewById(R.id.uploadProgress);

        autoCheckBox = (CheckBox) view.findViewById(R.id.check_settings);

        uploadButton = (Button) view.findViewById(R.id.UploadButton);
        quitButton = (Button) view.findViewById(R.id.QuitButton);
        pbUpload = (ProgressBar) view.findViewById(R.id.pbUpload);
        pbText = (TextView) view.findViewById(R.id.pbText);

        sdf = new SimpleDateFormat("yyyyMMddHHmm");
        sdfMilli = new SimpleDateFormat("yyyyMMddHHmmss");
        LogDate = new Date();

        updateViewHandler.post(updateView);

        uploadButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                if (RunIntentService.runFlag) {
                    if (checkNetworkStatus()) {
                        //-------------
                        String variables = "5";
                        HttpsConnection httpsconnection = new HttpsConnection(getActivity());
                        httpsconnection.setActivity(getActivity());
                        httpsconnection.setMethod("GET", variables);
                        httpsconnection.execute("/signal/api/appversion");

                        variables = "code=" + "psTc8qWfbyYfOknpJd92DoYE";
                        HttpsConnection httpsPostFKey = new HttpsConnection(getActivity());
                        httpsPostFKey.setActivity(getActivity());
                        httpsPostFKey.setMethod("POST", variables);
                        httpsPostFKey.execute("/signal/api/upload");
                        Log.d("FTP56", "result: "+httpsPostFKey.getResultKey());

                        SFTPController ftpController = new SFTPController();
                        ftpController.setProgressBar(pbUpload, pbText, uploadProgress);
                        ftpController.execute();

                    } else {
                        ShowDialogMsg.showDialog("Connection Error!");
                    }
                }
            }
        });

        quitButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                        .setMessage("Close App?")
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                            ShowDialogMsg.showDialog("Cancel");
                            }
                        })
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(getActivity(), MainActivity.class);

                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("LOGOUT", true);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        }).show();
            }
        });
        autoCheckBox.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                if(isChecked) {
                    UserConfig.setAutoUploadByMobile(true);
                } else {
                    UserConfig.setAutoUploadByMobile(false);
                }
                UserConfig.saveConfigTo(MainActivity.configPath);
            }
        });


        if (RunIntentService.runFlag == true) {
            setRunningText("APP is Running");
        }

        // Inflate the layout for this fragment
        return view;
    }

    public static void setRunningText(String str) {
        runningText.setText(str);
    }

    public boolean checkNetworkStatus() {
        NetworkInfo info = (NetworkInfo) ((ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        if (info!=null && info.isConnected()) {
            return true;
        }
        return false;
    }

    public static void showUploadSuccess(int type) {
        //type 1: manually press button
        //type 2: auto upload
        if (type == 1) {
        //    uploadProgress.setText("volume:" + SFTPController.uploadBytes / 1024 + " KB");
        }
        else if (type == 2) {
            uploadProgress.setText("auto upload:" + SFTPController.uploadBytes / 1024 + " KB");
        }
    }

}
