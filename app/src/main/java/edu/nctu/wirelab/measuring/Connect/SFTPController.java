package edu.nctu.wirelab.measuring.Connect;

import android.os.AsyncTask;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import java.io.File;
import java.util.Properties;

import edu.nctu.wirelab.measuring.Fragment.UserFragment;
import edu.nctu.wirelab.measuring.MainActivity;
import edu.nctu.wirelab.measuring.RunIntentService;
import edu.nctu.wirelab.measuring.ShowDialogMsg;

import static edu.nctu.wirelab.measuring.Connect.Setting.SFTP_HOST;
import static edu.nctu.wirelab.measuring.Connect.Setting.SFTP_PASSWORD;
import static edu.nctu.wirelab.measuring.Connect.Setting.SFTP_USERNAME;
import static edu.nctu.wirelab.measuring.Connect.Setting.UPLOADDATA_PATH;
import static edu.nctu.wirelab.measuring.Connect.Setting.UPLOADSIG_PATH;

/**
 * Use JSch to establish ssh(sftp) connection
 */

public class SFTPController extends AsyncTask<Void, String, Integer> { // Params, Progress, Result
    private final String tagName = "SFTPController";

    /**
     * JSch Session
     */
    private Session mSession;

    private ProgressBar pbUpload = null;
    private TextView pbText=null, uploadProgressText=null;
    public static int uploadBytes = 0;
    public static String logFile = "";

    public boolean connectSFTPServer(){
        try {
            JSch jsch = new JSch();
            mSession = null;
            mSession = jsch.getSession(SFTP_USERNAME, SFTP_HOST , 22); // port 22
            mSession.setPassword(SFTP_PASSWORD);
            Properties properties = new Properties();
            properties.setProperty("StrictHostKeyChecking", "no");
            mSession.setConfig(properties);
            mSession.connect();
        } catch (JSchException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void setProgressBar(ProgressBar pb, TextView pbT, TextView upT){
        pbUpload=pb; pbText=pbT; uploadProgressText=upT;
    }

    /**
     * Upload the data to sftp server
     * if there isn't "upload" word in the logs directory
     * @param params
     * @return
     */
    @Override
    protected Integer doInBackground(Void... params) {
        if(connectSFTPServer()){
            try {
                Channel channel = mSession.openChannel("sftp");
                channel.connect();
                ChannelSftp sftp = (ChannelSftp) channel;
                File folder = new File(MainActivity.logPath);
                String[] fileList = folder.list();
                if(fileList == null) {
                    return 0;
                }
                publishProgress("0");
                for(int i=0; i<fileList.length; i++){
                    if(!fileList[i].contains("uploaded") && !fileList[i].contains(RunIntentService.recordFilePrefix)){

                        if(fileList[i].contains(".sig")){
                            sftp.put(MainActivity.logPath+fileList[i], UPLOADSIG_PATH+fileList[i]);
                        }
                        else{
                            sftp.put(MainActivity.logPath+fileList[i], UPLOADDATA_PATH+fileList[i]);
                        }
                        File f = new File(MainActivity.logPath, fileList[i]);
                        uploadBytes += f.length();
                        f.renameTo(new File(MainActivity.logPath, "uploaded" + fileList[i]));
                        logFile = fileList[i];
                    }
                    float percent = (i/(float)fileList.length)*100;
                    publishProgress("" + (int)percent);
                }
                publishProgress("100");
                channel.disconnect();
                mSession.disconnect();
            } catch (JSchException e) {
                e.printStackTrace();
                return -1;
            }
            catch (SftpException e) {
                e.printStackTrace();
                return -1;
            }
            return 0;
        }
        return -1;
    }

    @Override
    protected void onPreExecute(){
    }

    @Override
    protected void onProgressUpdate(String... progress){
        if(pbUpload != null){
            pbUpload.setProgress(Integer.parseInt(progress[0]));
            pbText.setText(Integer.parseInt(progress[0]) + "%");
            uploadProgressText.setText("Uploading...");
        }
    }

    @Override
    protected void onPostExecute(Integer result){
        if(result == 0){
            removeUploadedInFolder(MainActivity.logPath);
            ShowDialogMsg.showDialogLong("Upload succeed");
            if(pbUpload != null) {
                UserFragment.showUploadSuccess(1);
            }
            else {
                UserFragment.showUploadSuccess(2);
            }
        }
        else {
            ShowDialogMsg.showDialog("Connection error");
        }
    }
    //remove all files containing "uploaded" in folderPath
    public void removeUploadedInFolder(String folderPath){
        File folder = new File(folderPath);
        String[] fileList = folder.list();
        for(int i=0; i<fileList.length; i++){
            if (fileList[i].contains("uploaded")){
                File file = new File(folderPath+fileList[i]);
                file.delete();
            }
        }
    }

    //Remove all files in folderPath
    public void removeFolder(String folderPath){
        File folder = new File(folderPath);
        String[] fileList = folder.list();
        for(int i=0; i<fileList.length; i++){
            File file = new File(folderPath + fileList[i]);
            file.delete();
        }
    }

    //Remove the FileName
    public void removeFile(String fileName){
        File file = new File(fileName);
        file.delete();
    }
}
