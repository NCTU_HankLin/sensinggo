package edu.nctu.wirelab.measuring;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import edu.nctu.wirelab.measuring.Fragment.LoginUserFragment;

public class UserConfig {

    public static String myUserName = null;
    public static String userGender = null, userBirthday = null, userEmail = null, userMsg = null;
    public static String [][] similarityArray ;


    public static boolean autoUploadByMobile = false;

    public static void setUserName(String username){
        myUserName = username;
    }

    public static void setUserInfo(JSONObject obj){
        try {
            userGender = obj.getString("gender");
            userBirthday = obj.getString("birthday");
            userEmail = obj.getString("email");
            userMsg = obj.getString("helloMsg");
            Log.d("0612", userMsg);

        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public static void setSocialInfo(JSONArray jsonArray){
        Log.d("0704", Integer.toString(jsonArray.length()));
        try {
            similarityArray = new String[jsonArray.length()][2];
            for (int i=0; i<jsonArray.length(); i++) {
                String tmp = "Similarity: ";
                JSONObject obj = jsonArray.getJSONObject(i);
                String id = obj.getString("userid");

                tmp += obj.getString("sim");

                similarityArray[i][0] = id;
                similarityArray[i][1] = tmp;
            }

        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public static void setAutoUploadByMobile(boolean autouploadbymobile){
        autoUploadByMobile = autouploadbymobile;
    }

    public static void saveConfigTo(String path){

        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(path));

            out.write("username=" + myUserName + "\n");
            out.write("AutoUploadByMobile=" + autoUploadByMobile + "\n");
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean loadConfigFrom(String path){
        //Log.d(TagName, "LoadConfigFrom: " + path);
        File file = new File(path);
        int flag = 0;

        if(!file.exists()){
            return false;
        }
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));

            String line;
            while ((line = br.readLine()) != null) {
                Log.d("0521", line);
                //Log.d(TagName, "line:"+line);
                String[] token = line.split("=");
                if (token[0].equals("username") && token.length>=2) {
                    //Log.d(TagName, "token[0].equals(\"email\")");
                    myUserName = token[1];
                    flag = flag | 1;
                    //Log.d(TagName, "myEmail:"+myEmail);
                } else if (token[0].equals("AutoUploadByMobile") && token.length>=2) {
                    if (token[1].compareTo("false") == 0) {
                        autoUploadByMobile = false;
                        flag = flag | 2;
                    } else if (token[1].compareTo("true") == 0) {
                        autoUploadByMobile = true;
                        flag = flag | 2;
                    }
                }
            }

            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(flag == 3) {
            return true;
        }
        else{
            //Log.d(TagName, "lack of email in the config or AutoUploadByMobile");
            return false;
        }
    }
}
