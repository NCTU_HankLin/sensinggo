package edu.nctu.wirelab.measuring.Fragment;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import edu.nctu.wirelab.measuring.Connect.HttpsConnection;
import edu.nctu.wirelab.measuring.File.JsonParser;
import edu.nctu.wirelab.measuring.R;
import edu.nctu.wirelab.measuring.ShowDialogMsg;
import edu.nctu.wirelab.measuring.UserConfig;
import edu.nctu.wirelab.measuring.CheckFormat;

/**
 * Created by py on 4/24/18.
 */

public class LoginUserFragment extends Fragment {
    public static EditText gender, birthday, email, helloMsg;
    public static TextView username;

    Calendar m_Calendar = Calendar.getInstance();

    private Button logoutBtn;
    private Button reviseBtn;

    private JsonParser jsonParser = null;
    public void setJsonParser(JsonParser json) {
        jsonParser = json;
    }

    private void showDatePickerDialog() {
        Calendar c = Calendar.getInstance();
        new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                m_Calendar.set(Calendar.YEAR, year);
                m_Calendar.set(Calendar.MONTH, monthOfYear);
                m_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy/MM/dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.TAIWAN);
                birthday.setText(sdf.format(m_Calendar.getTime()));
            }
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)).show();

    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_login_user, container, false);
        Log.d("0717","view test"+UserConfig.userEmail);
        birthday = (EditText) view.findViewById(R.id.birthdayEditText);
        email = (EditText) view.findViewById(R.id.emailEditText);
        gender = (EditText) view.findViewById(R.id.genderEditText);
        helloMsg = (EditText) view.findViewById(R.id.msgEditText);
        username = (TextView) view.findViewById(R.id.userName) ;

        reviseBtn = (Button) view.findViewById(R.id.reviseBtn);

        birthday.setText(UserConfig.userBirthday);
        username.setText(UserConfig.myUserName);
        gender.setText(UserConfig.userGender);
        helloMsg.setText(UserConfig.userMsg);
        email.setText(UserConfig.userEmail);

        birthday.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub
                if(hasFocus){
                    showDatePickerDialog();
                }
            }
        });

        birthday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showDatePickerDialog();
            }
        });


        reviseBtn.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view){
                String tmpEmail = email.getText().toString();
                if (CheckFormat.checkEmail(tmpEmail)) {
                    String reviseInfo = "username=" + username.getText().toString() +
                            "&birthday=" + birthday.getText().toString() +
                            "&email=" + email.getText().toString() +
                            "&gender=" + gender.getText().toString() +
                            "&helloMsg=" + helloMsg.getText().toString();
                    Log.d("0525", reviseInfo);

                    final HttpsConnection httpsConnection = new HttpsConnection(getActivity());
                    httpsConnection.setJsonParser(jsonParser);
                    httpsConnection.setActivity(getActivity());
                    httpsConnection.setMethod("POST", reviseInfo);
                    httpsConnection.execute("/updateUserInfo");

                }

                else {
                    ShowDialogMsg.showDialog("Please enter a valid e-mail.");
                }

            }
        });

        return view;

    }

    @Override
    public void onResume(){
        super.onResume();
        birthday.setText(UserConfig.userBirthday);
        username.setText(UserConfig.myUserName);
        gender.setText(UserConfig.userGender);
        helloMsg.setText(UserConfig.userMsg);
        email.setText(UserConfig.userEmail);
    }
}
