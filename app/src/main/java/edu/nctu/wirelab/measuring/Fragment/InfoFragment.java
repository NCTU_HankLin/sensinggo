package edu.nctu.wirelab.measuring.Fragment;

import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.os.Handler;
import android.widget.TextView;

import edu.nctu.wirelab.measuring.BroadcastReceiver.BatteryInfoReceiver;
import edu.nctu.wirelab.measuring.File.JsonParser;
import edu.nctu.wirelab.measuring.Measurement.Location;
import edu.nctu.wirelab.measuring.MainActivity;
import edu.nctu.wirelab.measuring.Measurement.PhoneState;
import edu.nctu.wirelab.measuring.Measurement.SensorList;
import edu.nctu.wirelab.measuring.Measurement.SignalStrength;
import edu.nctu.wirelab.measuring.R;
import edu.nctu.wirelab.measuring.RunIntentService;
import edu.nctu.wirelab.measuring.BroadcastReceiver.ScreenStateReceiver;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InfoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InfoFragment extends Fragment {
    private TextView mobileInfoContent, atbsInfoContent, PhoneStateContent, ServingWifiContent, NeighborCellInfoContent, AppsUsageContent, NeighborWifiContent, SensorsContent;
    private Handler updateTextHandler = new Handler();

    //-------------------
    private JsonParser JsonParser = null;

    public void setJsonParser(JsonParser json) {
        JsonParser = json;
    }

    // Update info every second
    private Runnable UpdateInfoRunnable = new Runnable() {
        public void run() {
            // Get Lat/Lng from GPS_PROVIDER
            String LatG, LngG, SpeedG;
            String ProviderG = LocationManager.GPS_PROVIDER;

            // Get Lat/Lng from NETWORK_PROVIDER
            String LatN, LngN, SpeedN;
            String ProviderN = LocationManager.NETWORK_PROVIDER;

            // Initial variable
            LatG = LngG = SpeedG = LatN = LngN = SpeedN = "unknown";

            // If LocationUpdate gets value, update Lat/Lng/Speed
            if (Location.userlocationG != null) {
                LatG = "" + Location.userlocationG.getLatitude();
                LngG = "" + Location.userlocationG.getLongitude();
                SpeedG = "" + Location.userlocationG.getSpeed();
            }
            if (Location.userlocationN != null) {
                LatN = "" + Location.userlocationN.getLatitude();
                LngN = "" + Location.userlocationN.getLongitude();
                SpeedN = "" + Location.userlocationN.getSpeed();
            }

            String str = "null";
            str = String.format("%s: %s\n" +
                            "%s: %s\n" +
                            "%s: %s\n" +
                            "%s: %s\n" +
                            "%s: %s\n" +
                            "%s: %s\n" +
                            "%s: %s\n" +
                            "----------------------------------\n" +
                            "%s: %s | " +
                            "%s: %s\n" +
                            "%s: %s\n" +
                            "%s: %s\n" +
                            "%s: %s\n" +
                            "----------------------------------\n" +
                            "%s: %s | " +
                            "%s: %s\n" +
                            "%s: %s\n" +
                            "%s: %s\n" +
                            "%s: %s\n" +
                            "----------------------------------\n" +
                            "%s: %s\n" +
                            "%s\n" +
                            "%s: %s\n" +
                            "%s: %s",
                    "AppVersion", MainActivity.VERSION,
                    "DataVersion", JsonParser.dataVersion,
                    "AndroidVersion", Build.VERSION.RELEASE,
                    "Account", JsonParser.account,
                    "equipmentID", RunIntentService.tm.getDeviceId(),
                    "Model", JsonParser.MODEL,
                    "BatteryLevel", BatteryInfoReceiver.electricity,
                    "Lat", LatG,
                    "Lng", LngG,
                    "Speed", SpeedG,
                    "Provider", ProviderG,
                    "GPS update time", Location.updateTimeStampG,
                    "Lat", LatN,
                    "Lng", LngN,
                    "Speed", SpeedN,
                    "Provider", ProviderN,
                    "GPS update time", Location.updateTimeStampN,
                    "Screen state", ScreenStateReceiver.screen_state,
                    RunIntentService.displayPhoneState(),
                    "NetworkType", RunIntentService.networkType,
                    "ConnectionState", RunIntentService.connectionState);

            mobileInfoContent.setText(str);

            if (SignalStrength.cellInfoType == null) {
                str = "InfoTypeContent";
            } else if (SignalStrength.cellInfoType.equals("LTE")) {
                str = String.format("%-10s: %-10s\n" +
                                "%s: %s\n" +
                                "%s: %s\n" +
                                "%s: %s\n" +
                                "%s: %s\n" +
                                "%s: %s\n" +
                                "%s: %s\n" +
                                "%s: %s\n" +
                                "%s: %s\n" +
                                "%s: %s",
                        "InfoType", SignalStrength.cellInfoType,
                        "CellID", SignalStrength.lteCellID,
                        "MCC", SignalStrength.lteCellMCC,
                        "MNC", SignalStrength.lteCellMNC,
                        "PCI", SignalStrength.lteCellPCI,
                        "TAC", SignalStrength.lteCellTAC,
                        "RSSI", SignalStrength.lteCellRSSI,
                        "SINR", "unknown",
                        "RSRQ", SignalStrength.lteCellRSRQ,
                        "RSRP", SignalStrength.lteCellRSRP
                );
            } else if (SignalStrength.cellInfoType.equals("Wcdma")) {
                str = String.format("%s: %s\n" +
                                "%s: %s\n" +
                                "%s: %s\n" +
                                "%s: %s\n" +
                                "%s: %s\n" +
                                "%s: %s\n" +
                                "%s: %s",
                        "InfoType", SignalStrength.cellInfoType,
                        "CellID", SignalStrength.wcdmaAtCellID,
                        "CellMCC", SignalStrength.wcdmaAtCellMCC,
                        "CellMNC", SignalStrength.wcdmaAtCellMNC,
                        "CellPSC", SignalStrength.wcdmaAtCellPsc,
                        "CellLAC", SignalStrength.wcdmaAtCellLac,
                        "SignalStrength", SignalStrength.wcdmaAtCellSignalStrength
                );
            }
            atbsInfoContent.setText(str);
            PhoneStateContent.setText(PhoneState.phoneState);

            if (JsonParser.appsInfo != null) {
                if (RunIntentService.servingWifiInfo!=null && RunIntentService.wifiState) {
                    ServingWifiContent.setText(RunIntentService.servingWifiInfo);
                } else {
                    ServingWifiContent.setText("null");
                }
            }

            if (RunIntentService.allWiFiInfo != null && RunIntentService.wifiState) {
                //NeighborWifiContent.setText(RunIntentService.AllWiFiInfo);
            } else {
                ServingWifiContent.setText("null");
            }

            float x = SensorList.gSensorValues[0];
            float y = SensorList.gSensorValues[1];
            float z = SensorList.gSensorValues[2];
            str = String.format("%s:\n%s\n" +
                            "%s: %s\n" +
                            "%s:\n%s\n" +
                            "%s:\n%s\n" +
                            "%s:\n%s\n" +
                            "%s: %s\n" +
                            "%s: %s\n" +
                            "%s:\n%s\n",
                    "Accelerometer(will be updated when the value is changed exceed 1)", "x:" + SensorList.gSensorValues[0] + " y:" + SensorList.gSensorValues[1] + " z:" + SensorList.gSensorValues[2],
                    "x y z=>",Math.sqrt(x*x+y*y+z*z),
                    "Azimuth(-z) 0=>N 90=>E 180/-180=>S -90=>W",""+SensorList.orienValue[0],
                    "Pitch(x) 0=>N 90=>E 180/-180=>S -90=>W",""+SensorList.orienValue[1],
                    "Roll(y) 0=>N 90=>E 180/-180=>S -90=>W",""+SensorList.orienValue[2],
                    "Light(will be updated when the value is changed more than 10)", SensorList.lightValue,
                    "Proximity(near|far)", SensorList.proximityValue,
                    "Magnetic field(will be updated when the value is changed more than 10)", "x:" + SensorList.magneticValues[0] + " y:" + SensorList.magneticValues[1] + " z:" + SensorList.magneticValues[2]
            );
            SensorsContent.setText(str);
            updateTextHandler.postDelayed(UpdateInfoRunnable, 1000);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);

        AppsUsageContent = (TextView) view.findViewById(R.id.AppsUsageContent);

        NeighborCellInfoContent = (TextView) view.findViewById(R.id.NeighborCellInfoContent);
        NeighborWifiContent = (TextView) view.findViewById(R.id.NeighborWifiContent);

        PhoneStateContent = (TextView) view.findViewById(R.id.PhoneStateContent);

        SensorsContent = (TextView) view.findViewById(R.id.SensorsContent);
        ServingWifiContent = (TextView) view.findViewById(R.id.ServingWifiContent);

        atbsInfoContent = (TextView) view.findViewById(R.id.AtBSInfoContent);
        mobileInfoContent = (TextView) view.findViewById(R.id.MobileInfoContent);

        updateTextHandler.post(UpdateInfoRunnable);
        // Inflate the layout for this fragment
        return view;
    }

}
