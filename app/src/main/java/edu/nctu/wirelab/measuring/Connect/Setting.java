package edu.nctu.wirelab.measuring.Connect;

public class Setting {
    public static final String HTTPS_SERVER = "your https server ip";
    public static final String SFTP_HOST = "sftp host ip";
    public static final String SFTP_PASSWORD = "password";
    public static final String SFTP_USERNAME = "username";
    public static final String UPLOADDATA_PATH = "sftp server data path";
    public static final String UPLOADSIG_PATH = "sftp server signature path";

}
